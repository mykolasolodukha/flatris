FROM node

# Optional: install nano editor
RUN apt-get update && apt-get install -y nano

# Copy ONLY package.json since `jarn install` requires only this file
# I.e. cached layers will be used if the file ramains unchanged
COPY package.json /app/
# Copy yarn.lock to get consistent installs across machines
COPY yarn.lock /app/
WORKDIR /app

RUN yarn install

# Copy all the remaining files
COPY . /app

RUN yarn test
RUN yarn build

# Start the app if no other command passed
CMD yarn start

EXPOSE 3000
